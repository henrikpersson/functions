#! /usr/bin/env python3.3
# 
# Wrapper script for runtest.py tester
# 
# Simplify running of test cases for
# patwic functional test exercises
#
#
import argparse
import sys
import os.path
from runtest import run_tests, print_results

TESTS_ZIP_FILE = "tests.zip"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run level tests')
    parser.add_argument('level', type=int, metavar='level', choices=range(1, 8),
       help='the test level to execute tests for')
    args = parser.parse_args()

    # check if level<n>.py exists, otherwise it futile....
    testfile = "level{}.py".format(args.level)
    if  not os.path.exists(testfile):
        print("Error: the {} file does not exist, please unlock it first.".format(testfile), file=sys.stderr)
        sys.exit(1)

    # append parent dir of where this script was found to allow
    # import to work from calling directory
    exec_dir=os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))
    sys.path.insert(0, exec_dir)

    # add tests.zip to sys.path if it is in the current directory
    if os.path.exists(TESTS_ZIP_FILE):
        sys.path.insert(0, TESTS_ZIP_FILE)

    # Run the test named: tests.level<d>_tests
    testname = "tests.level{}_tests".format(args.level)

    res = run_tests(testname)
    if res:
        print_results(res)
    else:
        sys.exit(1)
